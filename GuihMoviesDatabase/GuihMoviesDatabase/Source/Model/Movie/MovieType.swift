//
//  MovieType.swift
//  Example
//
//  Created by Guilherme Antunes Ferreira on 21/02/18.
//  Copyright © 2018 CI&T. All rights reserved.
//

import Foundation

enum MovieType : String {
    case popular
    case favorites
}
