//
//  Genre.swift
//  GuihMoviesDatabase
//
//  Created by Guilherme Antunes on 05/03/18.
//  Copyright © 2018 Guilherme Antunes. All rights reserved.
//

import UIKit

struct Genre: Codable {
    
    var identifier: Int?
    var name: String?
    
    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case name
    }
}
