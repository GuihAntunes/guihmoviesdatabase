//
//  MovieRequest.swift
//  Example
//
//  Created by Guilherme Antunes Ferreira on 21/02/18.
//  Copyright © 2018 CI&T. All rights reserved.
//

import Foundation

struct MovieRequest: Codable {
    
    let apiKey = ApiProvider.apiSecret
    
    enum CodingKeys: String, CodingKey {
        case apiKey = "api_key"
    }
}

