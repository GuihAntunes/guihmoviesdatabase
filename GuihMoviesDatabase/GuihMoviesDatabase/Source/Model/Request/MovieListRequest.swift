//
//  MovieListRequest.swift
//  Example
//
//  Created by Guilherme Antunes Ferreira on 22/02/18.
//  Copyright © 2018 CI&T. All rights reserved.
//

import UIKit

struct MovieListRequest: Codable {
    
    let apiKey = ApiProvider.apiSecret
    var page: Int?
    
    init(withPage page: Int = 1) {
        self.page = page
    }
    
    enum CodingKeys: String, CodingKey {
        case apiKey = "api_key"
        case page
    }
    
}
