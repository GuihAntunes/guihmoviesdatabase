//
//  HomeManager.swift
//  Example
//
//  Created by Guilherme Antunes Ferreira on 21/02/18.
//  Copyright © 2018 CI&T. All rights reserved.
//

import UIKit

class HomeManager : BaseManager {
    
    /// Movie Business
    private lazy var business: MovieBusiness = {
        return MovieBusiness()
    }()
    
    func popularMovies(movieType : MovieType, nextPage : Bool, _ completion : @escaping MovieListCallback) {
        addOperation {
            self.business.fetchMovies(movieType: movieType, nextPage : nextPage, completion: { (moviesList) in
                OperationQueue.main.addOperation {
                    completion(moviesList)
                }
            })
        }
    }
    
    public func saveMovie(_ movie : Movie, withImage image : UIImage) {
        self.business.saveMovie(movie, withImage: image)
    }
    
    public func getMoviesFromCache() -> FavoriteMoviesCallback {
        return self.business.getSavedMovies()
    }
    
    public func movieExistsOnCache(_ movie : Movie) -> Bool {
        return self.business.movieExistsOnCache(movie)
    }
    
    public func removeMovieFromCache(_ movie : Movie) {
        self.business.removeMovieFromCache(movie)
    }
    
}
