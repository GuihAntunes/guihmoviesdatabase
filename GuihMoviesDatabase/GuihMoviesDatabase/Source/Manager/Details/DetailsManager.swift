//
//  DetailsManager.swift
//  GuihMoviesDatabase
//
//  Created by Guilherme Antunes on 05/03/18.
//  Copyright © 2018 Guilherme Antunes. All rights reserved.
//

import Foundation
import UIKit

class DetailsManager : BaseManager {
    
    /// Movie Business
    private lazy var business: MovieBusiness = {
        return MovieBusiness()
    }()
    
    private var selectedMovie : Movie?
    
    public func fetchMovie(identifier : Int, completion: @escaping SelectedMovieCallback) {
        addOperation {
            self.business.fetchMovie(identifier: identifier, completion: { (movie) in
                OperationQueue.main.addOperation {
                    completion(movie)
                }
            })
        }
    }
    
    public func saveMovie(_ movie : Movie, withImage image : UIImage) {
        self.business.saveMovie(movie, withImage: image)
    }
    
    public func getMoviesFromCache() -> FavoriteMoviesCallback {
        return self.business.getSavedMovies()
    }
    
    public func movieExistsOnCache(_ movie : Movie) -> Bool {
        return self.business.movieExistsOnCache(movie)
    }
    
    public func removeMovieFromCache(_ movie : Movie) {
        self.business.removeMovieFromCache(movie)
    }
    
}
