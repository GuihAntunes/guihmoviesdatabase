//
//  MovieBusiness.swift
//  GuihMoviesDatabase
//
//  Created by Guilherme Antunes on 21/02/18.
//  Copyright © 2018 Guilherme Antunes. All rights reserved.
//

import Foundation
import Alamofire
import Cache

typealias MovieListCallback = (@escaping () -> MovieList?) -> Void
typealias SelectedMovieCallback = (@escaping () -> Movie?) -> Void
typealias FavoriteMoviesCallback = ([Movie], [UIImage])

class MovieBusiness {
    
    private var movies : MovieList?
    private let diskConfig = DiskConfig(name: "Movies")
    private let memoryConfig = MemoryConfig(expiry: .never, countLimit: 100, totalCostLimit: 100)
    private var storage : Storage?
    private let defaults = UserDefaults.standard
    
    private func updateCachedKeys(forKey key : String, shouldInclude : Bool) {
        
        var array : [String] = defaults.stringArray(forKey: "CachedKeys") ?? []
        
        if array.isEmpty && !shouldInclude {
            return
        }
        
        if array.contains(key) && shouldInclude {
            return
        }
        
        if !array.contains(key) && !shouldInclude {
            return
        }
        
        if (array.isEmpty || !array.contains(key)) && shouldInclude {
            array.append(key)
            defaults.set(array, forKey: "CachedKeys")
            return
        }
        
        if array.contains(key) && !shouldInclude {
            let index = array.index(of: key) ?? 0
            array.remove(at: index)
            defaults.set(array, forKey: "CachedKeys")
            return
        }
        
    }
    
    public func saveMovie(_ movie : Movie, withImage image : UIImage) {
        if storage == nil {
            storage = try? Storage(diskConfig: diskConfig, memoryConfig: memoryConfig)
        }
        guard let key = movie.originalTitle else { return }
        let keyForImage = key + "Image"
        let imageWrapper = ImageWrapper(image: image)
        try? storage?.setObject(imageWrapper, forKey: keyForImage)
        try? storage?.setObject(movie, forKey: key)
        updateCachedKeys(forKey: key, shouldInclude: true)
    }
    
    public func getSavedMovies() -> FavoriteMoviesCallback {
        if storage == nil {
            storage = try? Storage(diskConfig: diskConfig, memoryConfig: memoryConfig)
        }
        
        let cachedKeys : [String] = defaults.stringArray(forKey: "CachedKeys") ?? []
        
        var cachedMovies : FavoriteMoviesCallback = FavoriteMoviesCallback([], [])
        
        if !cachedKeys.isEmpty {
            cachedKeys.forEach { (key) in
                guard let movie = try? storage?.object(ofType: Movie.self, forKey: key) else { return }
                guard let image = try? storage?.object(ofType: ImageWrapper.self, forKey: key + "Image").image else { return }
                guard let unwrappedImage = image else { return }
                guard let unwrappedMovie = movie else { return }
                cachedMovies.0.append(unwrappedMovie)
                cachedMovies.1.append(unwrappedImage)
            }
            
            return cachedMovies
        }
        
        return FavoriteMoviesCallback([], [])
    }
    
    public func movieExistsOnCache(_ movie : Movie) -> Bool {
        if storage == nil {
            storage = try? Storage(diskConfig: diskConfig, memoryConfig: memoryConfig)
        }
        guard let key = movie.originalTitle,
            let movieExists = try? storage?.existsObject(ofType: Movie.self, forKey: key) else {
            return false
        }
        
        return movieExists ?? false
    }
    
    public func removeMovieFromCache(_ movie : Movie) {
        if storage == nil {
            storage = try? Storage(diskConfig: diskConfig, memoryConfig: memoryConfig)
        }
        guard let key = movie.originalTitle else { return }
        let keyForImage = key + "Image"
        updateCachedKeys(forKey: key, shouldInclude: false)
        
        try? storage?.removeObject(forKey: key)
        try? storage?.removeObject(forKey: keyForImage)
    }
    
    public func fetchMovies(movieType : MovieType, nextPage : Bool, completion : @escaping MovieListCallback) {
        if let currentUpcomingList = self.movies {
            let currentPage: Int = currentUpcomingList.page ?? 0
            let lastPage: Int = currentUpcomingList.totalPages ?? Int.max
            if currentPage >= lastPage {
                completion { currentUpcomingList }
                return
            }
            
            if nextPage {
                self.movies?.page = currentPage + 1
            }
        }
        
        let urlString = ApiProvider.baseUrl + movieType.rawValue + ApiProvider.apiSecret + ApiProvider.page + String(describing: self.movies?.page ?? 1)
        
        guard let url = URL(string: urlString) else {
            print("Failed to create url!")
            return
        }
        
        ApiProvider.requestForMovieList(url: url) { (movieList) in
            guard let movieList = movieList() else { return }
            
            if self.movies == nil {
                self.movies = movieList
            } else {
                self.append(movieList: movieList, to: &self.movies)
            }
            
            completion { self.movies }
            
        }
        
    }
    
    public func fetchMovie(identifier : Int, completion : @escaping SelectedMovieCallback) {
        
        let urlString = ApiProvider.baseUrl + String(describing: identifier) + ApiProvider.apiSecret
        
        guard let url = URL(string: urlString) else {
            print("Failed to create url!")
            return
        }
        
        ApiProvider.requestForMovie(url: url) { (movie) in
            guard let movie = movie() else { return }
            completion { movie }
        }
        
    }
    
    fileprivate func append(movieList newList: MovieList, to oldList: inout MovieList?) {
        guard var list = oldList else { return }
        
        list.page = newList.page
        if let results = newList.results {
            list.results?.append(contentsOf: results)
        }
        
        oldList = list
    }
    
}
