//
//  FavoriteMovieCollectionViewCell.swift
//  GuihMoviesDatabase
//
//  Created by Guilherme Antunes on 07/03/18.
//  Copyright © 2018 Guilherme Antunes. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class FavoriteMovieCollectionViewCell: UICollectionViewCell, Identifiable {
    // MARK: - Outlets
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var movieThumbNail: UIImageView!
    @IBOutlet weak var movieName: UILabel!
    
    // MARK: - Properties
    fileprivate var shadowLayer : CAShapeLayer!
    fileprivate let homeManager = HomeManager(maxConcurrentOperationCount: 500)
    fileprivate var movie : Movie?
    
    // MARK: - General Methods
    override func prepareForReuse() {
        super.prepareForReuse()
        movieThumbNail.af_cancelImageRequest()
        movieName.text? = ""
        movieThumbNail.contentMode = .center
        movieThumbNail.image = UIImage(named: "claquete")
        movieThumbNail.layer.roundCorners(radius: 10)
        movieThumbNail.clipsToBounds = true
        favoriteButton.imageView?.image = UIImage(named: "favorite_full_icon")
    }
    
    internal func initWithModel(movie : Movie) {
        self.movie = movie
        favoriteButton.imageView?.image = UIImage(named: "favorite_full_icon")
        movieName.text = movie.originalTitle
        setupImage(posterPath: movie.posterPath)
    }
    
    private func setupImage(posterPath : String?) {
        let placeholder = UIImage(named: "claquete")
        guard var urlPath = posterPath else {
            movieThumbNail.image = UIImage(named: "claquete")
            return
        }
        
        urlPath = ApiProvider.posterBaseUrl + urlPath
        
        guard let url = URL(string: urlPath) else {
            movieThumbNail.image = UIImage(named: "claquete")
            return
        }
        
        movieThumbNail.af_setImage(withURL: url, placeholderImage: placeholder, filter: nil, progress: nil, progressQueue: .global(), imageTransition: .flipFromTop(0.3), runImageTransitionIfCached: true) { (response) in
            self.movieThumbNail.contentMode = .scaleToFill
            self.movieThumbNail.clipsToBounds = true
            self.movieThumbNail.layer.roundCorners(radius: 10)
        }
    }
    
    // MARK: - Actions
    @IBAction func removeFavorite(sender : UIButton) {
        guard let selectedMovie = self.movie else { return }

        let image = UIImage(named: "favorite_empty_icon")
        favoriteButton.setImage(image, for: .normal)
        homeManager.removeMovieFromCache(selectedMovie)
    }
    
}


