//
//  MovieCollectionViewCell.swift
//  Example
//
//  Created by Guilherme Antunes Ferreira on 21/02/18.
//  Copyright © 2018 CI&T. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class MovieCollectionViewCell: UICollectionViewCell, Identifiable {
    // MARK: - Outlets
    @IBOutlet weak var movieThumbNail: UIImageView!
    @IBOutlet weak var movieName: UILabel!
    
    // MARK: - Properties
    fileprivate var shadowLayer : CAShapeLayer!
    fileprivate let homeManager = HomeManager(maxConcurrentOperationCount: 500)
    fileprivate var movie : Movie?
    
    // MARK: - General Methods
    override func prepareForReuse() {
        super.prepareForReuse()
        movieThumbNail.af_cancelImageRequest()
        movieName.text? = ""
        movieThumbNail.contentMode = .center
        movieThumbNail.image = UIImage(named: "claquete")
        movieThumbNail.layer.roundCorners(radius: 10)
        movieThumbNail.clipsToBounds = true
    }
    
    internal func initWithModel(movie : Movie) {
        self.movie = movie
        movieName.text = movie.originalTitle
        setupImage(posterPath: movie.posterPath)
    }
    
    internal func getSelectedMovie() -> Movie? {
        return self.movie
    }

    private func setupImage(posterPath : String?) {
        let placeholder = UIImage(named: "claquete")
        guard var urlPath = posterPath else {
            movieThumbNail.image = UIImage(named: "claquete")
            return
        }
        
        urlPath = ApiProvider.posterBaseUrl + urlPath
        
        guard let url = URL(string: urlPath) else {
            movieThumbNail.image = UIImage(named: "claquete")
            return
        }
        
        movieThumbNail.af_setImage(withURL: url, placeholderImage: placeholder, filter: nil, progress: nil, progressQueue: .global(), imageTransition: .flipFromTop(0.3), runImageTransitionIfCached: true) { (response) in
            self.movieThumbNail.contentMode = .scaleToFill
            self.movieThumbNail.clipsToBounds = true
            self.movieThumbNail.layer.roundCorners(radius: 10)
        }
    }
}
