//
//  CustomSegmentedControl.swift
//  GuihMoviesDatabase
//
//  Created by Guilherme Antunes on 06/03/18.
//  Copyright © 2018 Guilherme Antunes. All rights reserved.
//

import UIKit

@IBDesignable
class CustomSegmentedControl: UIView {
    
    var buttons : [UIButton] = []
    var selector : UIView!

    @IBInspectable
    var borderWidth : CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
    var borderColor : UIColor = .clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable
    var textColor : UIColor = .white {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable
    var selectedColor : UIColor = .white {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable
    var selectedTextColor : UIColor = .white {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable
    var commaSeparatedButtonTitles : String = "" {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        buttons.removeAll()
        subviews.forEach { $0.removeFromSuperview() }
        center = CGPoint(x: UIScreen.main.bounds.maxX / 2, y: UIScreen.main.bounds.maxY - 8)
        
        let buttonTitles = commaSeparatedButtonTitles.components(separatedBy: ",")
        buttonTitles.forEach { (title) in
            let button = UIButton(type: .system)
            button.setTitle(title, for: .normal)
            button.setTitleColor(textColor, for: .normal)
            buttons.append(button)
        }
        
        let selectorWidth = frame.width / CGFloat(buttonTitles.count)
        selector = UIView(frame: CGRect(x: 0, y: 0, width: selectorWidth, height: frame.height))
        selector.layer.roundCorners(radius: frame.height / 2)
        selector.backgroundColor = selectedColor
        addSubview(selector)
        
        let stackView = UIStackView(arrangedSubviews: buttons)
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillProportionally
        addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        frame.size.width = intrinsicContentSize.width + 16
        frame.size.height = intrinsicContentSize.height + 4
        
        stackView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        stackView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        stackView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        updateConstraints()
    }
    
    override func draw(_ rect: CGRect) {
        layer.cornerRadius = frame.height / 2
    }

}
