//
//  BackgroundView.swift
//  GuihMoviesDatabase
//
//  Created by Guilherme Antunes on 02/03/18.
//  Copyright © 2018 Guilherme Antunes. All rights reserved.
//

import UIKit

class BackgroundView: UIView {
    override func draw(_ rect: CGRect) {
        let gradientSubView = UIView(frame: rect)
        let gradientLayer = createBlueGradientLayer(ForFrame: gradientSubView.frame)
        gradientSubView.layer.insertSublayer(gradientLayer, at: 0)
        gradientSubView.alpha = 1
        self.addSubview(gradientSubView)
    }
}

extension UIView {
    func drawWithGradient(_ rect: CGRect) {
        let gradientSubView = UIView(frame: rect)
        let gradientLayer = createBlueGradientLayer(ForFrame: gradientSubView.frame)
        gradientSubView.layer.insertSublayer(gradientLayer, at: 0)
        gradientSubView.alpha = 1
        self.addSubview(gradientSubView)
    }
}
