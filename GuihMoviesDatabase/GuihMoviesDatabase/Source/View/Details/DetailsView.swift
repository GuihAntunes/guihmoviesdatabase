//
//  DetailsView.swift
//  GuihMoviesDatabase
//
//  Created by Guilherme Antunes on 05/03/18.
//  Copyright © 2018 Guilherme Antunes. All rights reserved.
//

import UIKit

class DetailsView: BackgroundView {

    // MARK: - Outlets
    @IBOutlet private weak var selectedMovieImage : UIImageView!
    @IBOutlet private weak var movieGenreLabel : UILabel!
    @IBOutlet private weak var favoriteButton : UIButton!
    @IBOutlet private weak var overviewtextView : UITextView!
    
    // MARK: - Properties
    fileprivate var selectedMovie : Movie?

    // MARK: - General Methods
    public func getPosterImage() -> UIImage {
        return selectedMovieImage.image ?? UIImage(named: "claquete") ?? UIImage()
    }
    
    public func bindScreen(withMovie movie : Movie) {
        bringSubview(toFront: selectedMovieImage)
        bringSubview(toFront: movieGenreLabel)
        bringSubview(toFront: favoriteButton)
        bringSubview(toFront: overviewtextView)
        selectedMovie = movie
        loadPoster()
        loadGenre()
        loadOverview()
    }
    
    public func setFavoriteButtonState(_ isFavorite : Bool) {
        var image : UIImage?
        if isFavorite {
            image = UIImage(named: "favorite_full_icon")
        } else {
            image = UIImage(named: "favorite_empty_icon")
        }
        favoriteButton.setImage(image, for: .normal)
    }
    
    private func loadPoster() {
        let placeholder = UIImage(named: "claquete")
        guard let movie = selectedMovie else { return }
        guard var urlPath = movie.posterPath else {
            selectedMovieImage.image = UIImage(named: "claquete")
            return
        }
        
        urlPath = ApiProvider.posterBaseUrl + urlPath
        
        guard let url = URL(string: urlPath) else {
            selectedMovieImage.image = UIImage(named: "claquete")
            return
        }
        
        startLoading(view: selectedMovieImage)
        selectedMovieImage.af_setImage(withURL: url, placeholderImage: placeholder, progressQueue: .global(), imageTransition: .flipFromTop(0.3), runImageTransitionIfCached: true) { (response) in
            stopLoading()
            self.selectedMovieImage.contentMode = .scaleToFill
            self.selectedMovieImage.clipsToBounds = true
            self.selectedMovieImage.layer.roundCorners(radius: 10)
        }
    }
    
    private func loadGenre() {
        guard let movie = selectedMovie else { return }
        movieGenreLabel.text = movie.genres?.first?.name ?? "Not specified"
    }
    
    private func loadOverview() {
        guard let movie = selectedMovie else { return }
        overviewtextView.text = movie.overview ?? "Not Specified"
    }
}
