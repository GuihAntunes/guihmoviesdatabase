//
//  ApiProvider.swift
//  Example
//
//  Created by Guilherme Antunes Ferreira on 21/02/18.
//  Copyright © 2018 CI&T. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

struct ApiProvider {
    
    /// API version
    static let apiVersion = "3"
    
    static let page = "&page="
    
    static let apiSecret = "?api_key=530dea27c211e00f4c65c5a45907e773"
    
    static var baseUrl: String {
        return "https://api.themoviedb.org/" + apiVersion + "/movie/"
    }
    
    /// poster base URL
    static var posterBaseUrl: String {
        return "https://image.tmdb.org/t/p/w500"
    }
    
    /// poster base URL
    static var bannerBaseUrl: String {
        return "https://image.tmdb.org/t/p/original"
    }
    
    /// profile base URL
    static var profileBaseUrl: String {
        return "https://image.tmdb.org/t/p/w185"
    }
    
    static func requestForMovieList(url : URL, completion: @escaping MovieListCallback) {
        Alamofire.request(url).validate().responseData { (response) in
            
            guard let data = response.data else {
                print("Failed to get data from API")
                return
            }
            
            guard let upcomingList = try? JSONDecoder().decode(MovieList.self, from: data) else {
                print("Failed to parse")
                return
            }
            
            completion { upcomingList }
            
        }
        
    }
    
    static func requestForMovie(url : URL, completion : @escaping SelectedMovieCallback) {
        Alamofire.request(url).validate().responseData { (response) in
            guard let data = response.data else {
                print("Failed to get data from API")
                return
            }
            
            guard let movie = try? JSONDecoder().decode(Movie.self, from: data) else {
                print("Failed to parse")
                return
            }
            
            completion { movie }
        }
    }
    
}
