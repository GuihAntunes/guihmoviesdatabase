//
//  HomeViewController.swift
//  Example
//
//  Created by Guilherme Antunes Ferreira on 21/02/18.
//  Copyright © 2018 CI&T. All rights reserved.
//

import UIKit
import Lottie

class HomeViewController: UICollectionViewController {
    
    // MARK: - Properties
    fileprivate var movieType : MovieType = .popular
    
    fileprivate var movies : [Movie]? {
        get {
            switch movieType {
            case .popular: return popularMovies
            case .favorites: return favoritesMovies
            }
        }
        set {
            switch movieType {
            case .favorites: favoritesMovies = newValue
            case .popular: popularMovies = newValue
            }
        }
    }
    
    fileprivate let homeManager = HomeManager(maxConcurrentOperationCount: 500)
    fileprivate var selectedMovie : Movie?
    fileprivate var cachedImages : [UIImage]?
    
    fileprivate var popularMovies : [Movie]? {
        didSet {
            if popularMovies != nil {
                collectionView?.reloadData()
            }
        }
    }
    
    fileprivate var favoritesMovies : [Movie]? {
        didSet {
            if favoritesMovies != nil {
                collectionView?.reloadData()
            }
        }
    }
    
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        loadMovies()
        collectionView?.dataSource = self
        collectionView?.allowsMultipleSelection = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadMovies()
        collectionView?.reloadData()
    }
    
    // MARK: - General Methods
    fileprivate func setupEmptyState() {
        let frame = CGRect(x: view.frame.width / 4 - 125, y: view.frame.width / 4 - 125, width: 200, height: 200)
        let emptyStateView = EmptyStateView(frame: frame)
        emptyStateView.center = view.center
        emptyStateView.setup()
        view.addSubview(emptyStateView)
        view.bringSubview(toFront: emptyStateView)
    }
    
    fileprivate func removeEmptyStateViewIfNeeded() {
        view.subviews.forEach { (subview) in
            if subview is EmptyStateView {
                subview.removeFromSuperview()
            }
        }
    }
    
    fileprivate func loadMovies(nextPage : Bool = false) {
        collectionView?.alpha = 1
        removeEmptyStateViewIfNeeded()
        switch movieType {
        case .popular:
            loadPopularMovies(nextPage: nextPage)
        case .favorites:
            loadFavoritesMovies()
        }
    }
    
    fileprivate func loadPopularMovies(nextPage : Bool = false) {
        startLoading(view: view)
        homeManager.popularMovies(movieType: .popular, nextPage: nextPage) { [weak self] (movieList) in
            guard let _self = self else { return }
            
            guard let movies = movieList()?.results else {
                _self.collectionView?.alpha = 0
                let errorAnimation = LOTAnimationView(name: "error_message")
                errorAnimation.frame = CGRect(x: _self.view.frame.width / 4 - 125, y: _self.view.frame.width / 4 - 125, width: 350, height: 350)
                errorAnimation.center = _self.view.center
                _self.view.addSubview(errorAnimation)
                errorAnimation.play()
                stopLoading()
                return
            }
            
            _self.popularMovies = movies
            stopLoading()
            
            if _self.popularMovies?.count == 0 {
                _self.setupEmptyState()
            }
        }
    }
    
    fileprivate func clearFavoritesIfNeeded() {
        if favoritesMovies != nil {
            favoritesMovies!.removeAll()
        }
    }
    
    fileprivate func loadFavoritesMovies() {
        clearFavoritesIfNeeded()
        favoritesMovies = homeManager.getMoviesFromCache().0
        
        if favoritesMovies == nil || favoritesMovies?.isEmpty ?? true {
            setupEmptyState()
        }
    }
    
    // MARK: - Actions
    @IBAction func movieTypeChanged(_ sender: UISegmentedControl) {
        clearFavoritesIfNeeded()
        removeEmptyStateViewIfNeeded()
        if sender.selectedSegmentIndex == 0 {
            movieType = .popular
            title = "Popular Movies"
        } else {
            movieType = .favorites
            title = "Favorites"
        }
        loadMovies()
        collectionView?.reloadData()
    }
}

// MARK: - DataSource
extension HomeViewController : Identifiable {
    
    @objc func unfavoriteButtonClicked(_ sender : Any) {
        collectionView?.reloadData()
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let movie = movies?[indexPath.item] else { return UICollectionViewCell() }

        if movieType == .favorites {
            let cell : FavoriteMovieCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath)
            cell.initWithModel(movie: movie)
            guard let images = cachedImages else { return cell }
            cell.movieThumbNail.image = images[indexPath.item]
            cell.favoriteButton.addTarget(self, action: #selector(HomeViewController.unfavoriteButtonClicked(_:)), for: .touchUpInside)
            return cell
        }
        
        if movieType == .popular {
            let cell : MovieCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath)
            cell.initWithModel(movie: movie)
            if indexPath.item >= (movies?.count ?? 0) - 1 {
                loadMovies(nextPage: true)
            }
            return cell
        }

        return UICollectionViewCell()
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 didEndDisplaying cell: UICollectionViewCell,
                                 forItemAt indexPath: IndexPath) {
        guard let movieCell = cell as? MovieCollectionViewCell else { return }
        movieCell.movieThumbNail.af_cancelImageRequest()
    }
}

// MARK: - Delegate
extension HomeViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let detailsController = segue.destination as? DetailsViewController,
            let selectedIndexPath = collectionView?.indexPathsForSelectedItems?.first else {
                return
        }
        
        detailsController.transitioningDelegate = self
        detailsController.modalPresentationStyle = .custom
        detailsController.movieId = movies?[selectedIndexPath.item].identifier
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let movie = movies?[indexPath.item] else {
            return
        }
        selectedMovie = movie
        performSegue(withIdentifier: DetailsViewController.segueIdentifier, sender: nil)
    }
    
}

// MARK: - Delegate Flow Layout
extension HomeViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let margin = 20
        
        let width = (UIScreen.main.bounds.width - CGFloat(2*margin))/3
        
        let size = CGSize(width: width,
                          height: (width*1.41) + 30)
        
        return size
        
    }
    
}
