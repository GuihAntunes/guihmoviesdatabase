//
//  SplashScreenViewController.swift
//  GuihMoviesDatabase
//
//  Created by Guilherme Antunes on 27/02/18.
//  Copyright © 2018 Guilherme Antunes. All rights reserved.
//

import UIKit
import Lottie

class SplashScreenViewController: UIViewController {

    // MARK: - Properties
    lazy var initialAnimation = LOTAnimationView(name: "video_cam")
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        playInitialAnimation()
    }
    
    // MARK: - General Methods
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let homeController = segue.destination as? UINavigationController else {
            return
        }
        homeController.transitioningDelegate = self
        homeController.modalPresentationStyle = .custom
    }
    
    func playInitialAnimation() {
        self.applyGradientWithAlphaAnimation {
            self.playLottieAnimation()
        }
    }

    func playLottieAnimation() {
        initialAnimation.autoReverseAnimation = true
        initialAnimation.frame = CGRect(x: view.frame.width / 4 - 125, y: view.frame.width / 4 - 125, width: 350, height: 350)
        initialAnimation.center = view.center
        view.addSubview(initialAnimation)
        initialAnimation.play { (success) in
            if success {
                self.initialAnimation.alpha = 0
                self.performSegue(withIdentifier: HomeViewController.segueIdentifier, sender: nil)
            }
        }
    }
    
    func applyGradientWithAlphaAnimation(completion : @escaping () -> Void) {
        let gradientSubView = UIView(frame: self.view.frame)
        let gradientLayer = createBlueGradientLayer(ForFrame: gradientSubView.frame)
        gradientSubView.layer.insertSublayer(gradientLayer, at: 0)
        gradientSubView.alpha = 0
        view.addSubview(gradientSubView)
        UIView.animate(withDuration: 0.7, animations: {
            self.view.subviews.forEach({ (subview) in
                if subview == gradientSubView {
                    subview.alpha = 1
                }
            })
        }) { (success) in
            if success {
                completion()
            }
        }
    }
    
    // MARK: - Actions
}
