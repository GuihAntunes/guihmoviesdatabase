//
//  DetailsViewController.swift
//  GuihMoviesDatabase
//
//  Created by Guilherme Antunes on 05/03/18.
//  Copyright © 2018 Guilherme Antunes. All rights reserved.
//

import UIKit
import Lottie

class DetailsViewController: UIViewController, Identifiable, ViewCustomizable {
    
    // MARK: - Custom View
    typealias CustomView = DetailsView

    // MARK: - Properties
    public var movieId : Int?
    private var movie : Movie?
    private lazy var detailsManager : DetailsManager = {
        return DetailsManager()
    }()
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        loadMovie()
    }
    
    // MARK: - General Methods
    private func bindScreen() {
        guard let movie = self.movie else { return }
        customView.bindScreen(withMovie: movie)
        customView.setFavoriteButtonState(detailsManager.movieExistsOnCache(movie))
    }
    
    private func loadMovie() {
        guard let movieId = self.movieId else { return }
        startLoading(view: customView)
        detailsManager.fetchMovie(identifier: movieId) { [weak self] (movie) in
            guard let _self = self else { return }
            
            guard let movie = movie() else {
                let errorAnimation = LOTAnimationView(name: "error_message")
                errorAnimation.frame = CGRect(x: _self.view.frame.width / 4 - 125, y: _self.view.frame.width / 4 - 125, width: 350, height: 350)
                errorAnimation.center = _self.view.center
                _self.view.addSubview(errorAnimation)
                errorAnimation.play()
                stopLoading()
                return
            }
            
            _self.movie = movie
            _self.title = movie.originalTitle
            _self.bindScreen()
        }
    }
    
    // MARK: - Actions
    @IBAction private func setFavoriteMovie(sender : UIButton) {
        guard let movie = self.movie else { return }
        
        if detailsManager.movieExistsOnCache(movie) {
            customView.setFavoriteButtonState(false)
            detailsManager.removeMovieFromCache(movie)
        } else {
            customView.setFavoriteButtonState(true)
            detailsManager.saveMovie(movie, withImage: customView.getPosterImage())
        }
    }

}
